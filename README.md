EPICS IOC which talks to MOXA ioLogik E1260 using MODBUS/TCP The IOC uses open-source SW developed by EPICS community (see licensing information on: http://aps.anl.gov/epics/license/index.php ; http://www.aps.anl.gov/bcda/synApps/license.php

IOC is built with standard EPICS-base 3.14.12.3 package. It links modbus v2.4 device support module as it is found in synApps v5.7 package (see MX_E1260_ModbusIOCApp/src/Makefile).
File st.cmd configures modbus (asyn) interfaces to ioLogik E1260 in a way described by modbus module documentation. Modbus registers of E1260 are listed in MOXA ioLogic User manual.
EPICS records which read Modbus registers are defined in MX_E1260_ModbusIOCApp/Db/ioLogik_E1260.substitutions file (one per register). Template definition in use comes from modbus module distribution.
Records with RTD raw values, temperature values and averaged temperature values are available.

In CSS directory, a few Control System Studio GUI files can be found. OPI file moxa-ioLogik-E1260-all.opi displays expert overview of E1260 and teploty.opi plots temperature readings only in graph form.

Before compiling the IOC, modify configure/RELEASE file to match directory paths of your EPICS-base and support distribution.
Before running, modify st.cmd to match IP address of E1260.



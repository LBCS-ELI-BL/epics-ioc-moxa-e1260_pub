#######################################################################
# 1. SYSNAME: pickup one of listed here and uncomment (and comment out epicsEnvSet("SYSNAME","TEST") line below)
# 2. E124[0-9]_ASYNPORT: set this ASYN port name variable based on last byte of IP address, 
# 3. call drvAsynIPPortConfigure( asyn_port_name, IP_addres, ...), uncomment from predefined
# 4. call modbusInterposeConfig( asyn_port_name, .... some params, uncomment from predefined
# 5. for each modbus-register-access-port call drvModbusAsynConfigure(), uncomment from predefined
# 6. optionally: enable debug asynSetTraceMask() & asynSetTraceIOMask() on an arbitrary asyn- or modbus-port
# 7. edit TOP/MX_E12xx_ModbusIOCApp/Db/ substitution files in concern AND make rebuild
# 8. comment/uncomment dbLoadRecords() for desired db-files
#######################################################################

# MOXA ioLogik E1260 (6RTD), 

epicsEnvSet("SYSNAME","TEST")

#######################################################################
# set ASYN port name variable ('RIO' + last_byte_of_IP_address)

# ### E1260 (6RTD) ###
epicsEnvSet("E1260_ASYNPORT","RIO254")

drvAsynIPPortConfigure("$(E1260_ASYNPORT)", "192.168.127.254:502", 0, 0, 1)

#######################################################################
#drvAsynIPPortConfigure(const char *portName,
#                       const char *hostInfo,
#                       unsigned int priority,
#                       int noAutoConnect,
#                       int noProcessEos);

#######################################################################
# modbusInterposeConfig(const char *portName,
#                      modbusLinkType linkType, .... Modbus link layer type: 0 = TCP/IP , 1 = RTU, 2 = ASCII
#                      int timeoutMsec, 
#                      int writeDelayMsec)
modbusInterposeConfig("$(E1260_ASYNPORT)", 0, 2000, 0)

#######################################################################
# modbus port driver is created with the following command:
#drvModbusAsynConfigure(portName, 
#                        tcpPortName,
#                        slaveAddress, 
#                        modbusFunction, 
#                        modbusStartAddress, 
#                        modbusLength,
#                        dataType,
#                        pollMsec, 
#                        plcType);
#######################################################################

##Modbus functions
#Function name                          Function code
#Read Discrete Inputs                    2
#Read Coils                              1
#Write Single Coil                       5
#Write Multiple Coils                   15
#Read Input Registers                    4
#Read Holding Registers                  3
#Write Single Register                   6
#Write Multiple Registers               16
#Read/Write Multiple Registers          23
#Mask Write Register                    22
#Read FIFO Queue                        24
#Read File Record                       20
#Write File Record                      21
#Read Exception Status                   7
#Diagnostic                              8
#Get Com Event Counter                  11
#Get Com Event Log                      12
#Report Slave ID                        17
#Read Device Identification             43
#Encapsulated Interface Transport       43
#
#######################################################################

#######################################################################
#Supported Modbus data types
#modbusDataType value 	drvUser field 	Description
#0 	UINT16 	Unsigned 16-bit binary integers
#1 	INT16SM 	16-bit binary integers, sign and magnitude format. In this format bit 15 is the sign bit, and bits 0-14 are the absolute value of the magnitude of the number. 
#			This is one of the formats used, for example, by Koyo PLCs for numbers such as ADC conversions.
#2 	BCD_UNSIGNED 	Binary coded decimal (BCD), unsigned. This data type is for a 16-bit number consisting of 4 4-bit nibbles, each of which encodes a decimal number from 0-9. 
#			A BCD number can thus store numbers from 0 to 9999. Many PLCs store some numbers in BCD format.
#3 	BCD_SIGNED 	4-digit binary coded decimal (BCD), signed. This data type is for a 16-bit number consisting of 3 4-bit nibbles, and one 3-bit nibble. 
#			Bit 15 is a sign bit. Signed BCD numbers can hold values from -7999 to +7999. This is one of the formats used by Koyo PLCs for numbers such as ADC conversions.
#4 	INT16 		16-bit signed (2's complement) integers. This data type extends the sign bit when converting to epicsInt32.
#5 	INT32_LE 	32-bit integers, little endian (least significant word at Modbus address N, most significant word at Modbus address N+1)
#6 	INT32_BE 	32-bit integers, big endian (most significant word at Modbus address N, least significant word at Modbus address N+1)
#7 	FLOAT32_LE 	32-bit floating point, little endian (least significant word at Modbus address N, most significant word at Modbus address N+1)
#8 	FLOAT32_BE 	32-bit floating point, big endian (most significant word at Modbus address N, least significant word at Modbus address N+1)
#9 	FLOAT64_LE 	64-bit floating point, little endian (least significant word at Modbus address N, most significant word at Modbus address N+3)
#10 	FLOAT64_BE 	64-bit floating point, big endian (most significant word at Modbus address N, least significant word at Modbus address N+3)
#######################################################################

# MOXA E1260 RTDs: function 4 (Read Input Registers), address 0x600, 0x6 words , data_type = INT16 = 4 (its 2's complement)
drvModbusAsynConfigure("$(E1260_ASYNPORT)_RTD", "$(E1260_ASYNPORT)", 0, 4, 0x600, 0x6, 4, 500, "ioLogik")

##################################

# MOXA E1260 doesnt have a Watchdog

##################################
# modbus messages can be debuged here

# Enable ASYN_TRACEIO_HEX on octet server
#asynSetTraceIOMask("$(E1260_ASYNPORT)",0,4)
# Enable ASYN_TRACE_ERROR and ASYN_TRACEIO_DRIVER on octet server
#asynSetTraceMask("$(E1260_ASYNPORT)",0,9)

#asynSetTraceIOMask("$(E1260_ASYNPORT)_RTD",0,4)
#asynSetTraceMask("$(E1260_ASYNPORT)_RTD",0,9)

# ###################################################################
# ###################################################################
#
# comment out lines in particular substitutions-files in TOP/MX_E12xx_ModbusIOCApp/Db  and make rebuild
dbLoadRecords("../../db/ioLogik_E1260-addons.db","NAME=$(SYSNAME), ASYNPORT=$(E1260_ASYNPORT)")
dbLoadRecords("../../db/ioLogik_E1260.db","NAME=$(SYSNAME), ASYNPORT=$(E1260_ASYNPORT)")

# Application (not ioLogik itself) dependent addons go here, e.g.: archiver deadband (ADEL) field settings
# dbLoadRecords("MX_E12xx_app_dep_addons.template","NAME=$(SYSNAME), ASYNPORT=$(E1260_ASYNPORT)")

